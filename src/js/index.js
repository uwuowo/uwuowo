'use strict';

const UWU_STATES = {
    WAITING: 0,
    ACTIVATED: 1,
    COMPLETE: 2
};

/**
 * Returns the CSS property value of the given name on the target element.
 * @param {String} name - CSS property name
 * @param {HTMLElement} el = document.documentElement - Target element
 * @returns {String | undefined} CSS property value
 */
const getProperty = (name, el = document.documentElement) => {
    return getComputedStyle(el, null).getPropertyValue(name);
};

/**
 * Sets the CSS property value on the target element.
 * @param {String} name - CSS property name
 * @param {String} value - CSS property value
 * @param {HTMLElement} el = document.documentElement - Target element
 */
const setProperty = (name, value, el = document.documentElement) => {
    el.style.setProperty(name, value);
};

/**
 * Appends a px suffix to a value for use as a CSS size.
 * @param {Number} value
 * @returns {String} value with "px" appended
 */
const pxSuffix = (value) => {
    return String(value) + "px";
};

/**
 * Activates the owo transformation.
 */
const activateowo = () => {
    if (uwustate == UWU_STATES.WAITING) {
        uwustate = UWU_STATES.ACTIVATED;
        document.body.classList.add("goingowo");
        uwu.classList.add("goingowo");
        lasttime = new Date();
        starttime = lasttime;
        setTimeout(advanceowo, time);
    }
};

/**
 * Advances the owo transformation.
 */
const advanceowo = () => {
    if (step < steps) {
        const currtime = new Date();
        accumulator += currtime - lasttime;
        lasttime = currtime;
        
        while (accumulator >= time && step < steps) {
            step += 1;
            
            setProperty("--growoffset", pxSuffix(stepSize * step));
            
            glMatrix.vec2.random(offset, magnitude * step);
            setProperty("--xoffset", pxSuffix(offset[0]));
            setProperty("--yoffset", pxSuffix(offset[1]));
            
            accumulator -= time;
        }
        
        setTimeout(advanceowo, time);
    }
    else {
        finishowo();
    }
};

/**
 * Finishes the owo transformation.
 */
const finishowo = () => {
    uwustate = UWU_STATES.COMPLETE;
    setProperty("--xoffset", "0px");
    setProperty("--yoffset", "0px");
    setProperty("--growoffset", pxSuffix(stepSize * step));
    document.body.classList.remove("goingowo");
    uwu.classList.remove("goingowo");
    document.body.classList.add("owo");
    uwu.classList.add("owo");
    uwu.textContent = "owo";
    document.title = "owo";
    
    console.log(starttime, lasttime, lasttime - starttime);
};

const uwu = document.getElementById("uwu");
let uwustate = UWU_STATES.WAITING;

const steps = 1000;
const stepSize = (document.body.offsetHeight * 0.9 * 0.5 - uwu.offsetHeight * 0.5) / steps;
let step = 0;
const seconds = 8;
const time = seconds * 1000 / steps; // Milliseconds
const magnitude = 50 / steps;
let offset = glMatrix.vec2.create();
let accumulator = 0;
let starttime = null;
let lasttime = null;

uwu.addEventListener("mousedown", activateowo);
